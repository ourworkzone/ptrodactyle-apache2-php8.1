#!/bin/bash

echo "Starting PHP-FPM..."

/usr/sbin/php-fpm8.1 --fpm-config /home/container/php-fpm/php-fpm.conf --daemonize

export PHPRC=/home/container/php-fpm/php.ini

echo "Starting apache..."

/usr/sbin/apache2 -f /home/container/apache2/apache2.conf

chmod -R 777 /home/container/whmcs/*

echo "Starting Website..."

service php8.1-fpm status
php -v
apache2 -v

#/bin/bash -c "trap : TERM INT; sleep infinity & wait"
tail -f /home/container/other_vhosts_access.log
